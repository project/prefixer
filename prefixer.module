<?php

/**
 * @file
 * Primary module hooks for Prefixer module.
 */

/**
 * Implements hook_menu().
 */
function prefixer_menu() {
  $items['admin/config/development/prefixer'] = array(
    'title' => 'Prefixer',
    'description' => 'Manage prefixes.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prefixer_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'prefixer.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Field UI module adds hardcoded 'field_' prefix to field names so
 * we take some special steps to override it.
 *
 * @see field_ui_field_overview_form()
 */
function prefixer_form_field_ui_field_overview_form_alter(&$form, &$form_state, $form_id) {
  $form['fields']['_add_new_field']['field_name']['#field_prefix'] = '<span dir="ltr">' . prefixer_get_prefix('_prefixer_field_name_exists');
  $form['fields']['_add_new_field']['field_name']['#machine_name']['exists'] = '_prefixer_field_name_exists';
  $form['#validate'][] = '_prefixer_field_overview_form_validate';
}

/**
 * Replacement for _field_ui_field_name_exists().
 *
 * @see prefixer_form_field_ui_field_overview_form_alter()
 * @see _field_ui_field_name_exists()
 */
function _prefixer_field_name_exists($value) {
  $field_name = prefixer_get_prefix('_prefixer_field_name_exists') . $value;
  return (bool) field_read_fields(array('field_name' => $field_name), array('include_inactive' => TRUE));
}

/**
 * Form validation handler for field_ui_field_overview_form().
 *
 * @see _field_ui_field_overview_form_validate_add_new()
 */
function _prefixer_field_overview_form_validate($form, &$form_state) {
  // Remove 'field_' prefix that was earlier set
  // in _field_ui_field_overview_form_validate_add_new().
  $field_name = &$form_state['values']['fields']['_add_new_field']['field_name'];
  $field_name = preg_replace('/^field_/', '', $field_name);
}

/**
 * Implements hook_js_alter().
 */
function prefixer_js_alter(&$javascript) {
  if (isset($javascript['misc/machine-name.js'])) {
    // Swap out machine-name.js to use a modified version.
    $javascript['misc/machine-name.js']['data'] = drupal_get_path('module', 'prefixer') . '/prefixer.machine-name.js';
    // Pass field prefixes to frontend side.
    foreach ($javascript['settings']['data'] as $key => &$data) {
      if (isset($data['machineName'])) {
        // The page may contain multiple machine name elements.
        foreach ($data['machineName'] as &$machine_name_element) {
          $machine_name_element['field_prefix'] = prefixer_get_prefix($machine_name_element['exists']);
        }
      }
    }
  }
}

/**
 * Returns element prefix.
 */
function prefixer_get_prefix($exists_callback) {
  if (variable_get('prefixer_enable_global')) {
    return variable_get('prefixer_global_prefix');
  }
  else {
    $custom_prefixes = variable_get('prefixer_custom');
    return isset($custom_prefixes[$exists_callback]) ?
      $custom_prefixes[$exists_callback] : '';
  }
}

<?php

/**
 * @file
 * Admin page callbacks for the Prefixer module.
 */

/**
 * Form constructor for the prefixer configuration form.
 */
function prefixer_settings_form($form, &$form_state) {

  $form['prefixer_enable_global'] = array(
    '#type' => 'checkbox',
    '#title' => t('Global prefix'),
    '#default_value' => variable_get('prefixer_enable_global'),
  );

  $form['prefixer_global_prefix'] = array(
    '#type' => 'machine_name',
    '#title' => t('Global prefix'),
    '#description' => t('A machine-readable name.'),
    '#size' => 50,
    '#required' => FALSE,
    '#default_value' => variable_get('prefixer_global_prefix'),
    '#machine_name' => array(
      'exists' => 'prefixer_admin_machine_name_exists',
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="prefixer_enable_global"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Elements from core modules.
  $elements['node_type_load'] = array(
    'label' => t('Content type'),
    'module' => t('node'),
  );
  $elements['menu_edit_menu_name_exists'] = array(
    'label' => t('Menu'),
    'module' => t('menu'),
  );
  $elements['filter_format_exists'] = array(
    'label' => t('Text format'),
    'module' => t('filter'),
  );
  $elements['image_style_load'] = array(
    'label' => t('Image style'),
    'module' => t('image'),
  );
  $elements['system_get_date_types'] = array(
    'label' => t('Date type'),
    'module' => t('system'),
  );
  $elements['taxonomy_vocabulary_machine_name_load'] = array(
    'label' => t('Vocabulary'),
    'module' => t('taxonomy'),
  );
  $elements['_prefixer_field_name_exists'] = array(
    'label' => t('Field'),
    'module' => t('field_ui'),
  );

  // Elements from contributed modules.
  $elements['message_type_load'] = array(
    'label' => t('Message type'),
    'module' => t('message'),
  );
  $elements['views_get_view'] = array(
    'label' => t('View'),
    'module' => t('views'),
  );
  $elements['page_manager_page_load'] = array(
    'label' => t('Page'),
    'module' => t('page_manager'),
  );
  $elements['rules_config_load'] = array(
    'label' => t('Rule'),
    'module' => t('rules'),
  );
  $elements['commerce_product_type_load'] = array(
    'label' => t('Product type'),
    'module' => t('commerce_product'),
  );
  $elements['commerce_tax_rate_load'] = array(
    'label' => t('Tax rate'),
    'module' => t('commerce_tax_ui'),
  );
  $elements['features_export_form_module_name_exists'] = array(
    'label' => t('Feature'),
    'module' => t('features'),
  );
  $elements['_eck_fake_exists'] = array(
    'label' => t('Entity type'),
    'module' => t('eck'),
  );

  $custom_prefixes = variable_get('prefixer_custom');

  $form['prefixer_custom']['#tree'] = TRUE;
  foreach ($elements as $exists_callback => $element) {
    $title = t(
      '!label prefix (%module module)',
      array('!label' => $element['label'], '%module' => $element['module'])
    );
    $form['prefixer_custom'][$exists_callback] = array(
      '#type' => 'machine_name',
      '#title' => $title,
      '#description' => FALSE,
      '#default_value' => isset($custom_prefixes[$exists_callback]) ? $custom_prefixes[$exists_callback] : '',
      '#size' => 50,
      '#required' => FALSE,
      '#access' => module_exists($element['module']),
      '#machine_name' => array(
        'exists' => 'prefixer_admin_machine_name_exists',
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="prefixer_enable_global"]' => array('checked' => FALSE),
        ),
      ),
    );
  }

  return system_settings_form($form);
}

/**
 * We do not need prefixes to be unique.
 */
function prefixer_admin_machine_name_exists() {
  return FALSE;
}

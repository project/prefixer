Prefixer Drupal module
======================

Prefixer is a simple module that allows you to manage machine name prefixes.

After enabling the module navigate to admin/config/development/prefixer and set up required prefixes for the site.
